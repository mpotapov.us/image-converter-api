import express from 'express';
import { routes as imagesRoutes } from './images';

const routes = express.Router();

routes.use('/images', imagesRoutes);

export { routes };
