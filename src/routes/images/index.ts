import { Router } from 'express';
import { imageUploadController } from './controllers/image-upload/image-upload.controller';
import { imageDownloadController } from './controllers/image-download/image-download.controller';
import { imageStatusController } from './controllers/image-status/image-status.controller';
import { imageUploadMiddleware } from '../../midlewares/image-upload.middleware';
import { imageUploadValidator } from './controllers/image-upload/image-upload-validator';

const routes = Router();

routes.get('/status/:id', imageStatusController);

routes.get('/download/:id', imageDownloadController);

routes.post('/upload', imageUploadMiddleware, imageUploadValidator, imageUploadController);

export { routes };
