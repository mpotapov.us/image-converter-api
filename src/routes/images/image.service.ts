import { pipeline } from 'stream/promises';
import { createReadStream, createWriteStream } from 'fs';
import sharp, { AvailableFormatInfo, FormatEnum } from 'sharp';

type SupportedFormat = keyof FormatEnum | AvailableFormatInfo;

const convertImageToFormat = (src: string, out: string, format: SupportedFormat) => {
  const convert = sharp().toFormat(format);
  const read = createReadStream(src);
  const write = createWriteStream(out);
  return pipeline(read, convert, write);
};

export { convertImageToFormat, SupportedFormat };
