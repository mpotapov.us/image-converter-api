import { NextFunction, Request, Response } from 'express';
import { imageMetaStorage } from '../../../../common/image-meta.storage';
import { NotFoundException } from '../../../../common/exceptions';

const imageStatusController = (req: Request, res: Response, next: NextFunction) => {
  const id = req.params.id;
  const meta = imageMetaStorage.get(id);

  if (!meta) {
    return next(new NotFoundException());
  }

  return res.json({ status: meta.processing.status });
};

export { imageStatusController };
