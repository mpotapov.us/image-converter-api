import { join } from 'node:path';
import { createReadStream } from 'fs';
import { NextFunction, Request, Response } from 'express';
import { Config } from '../../../../config/config';
import { NotFoundException } from '../../../../common/exceptions';
import { imageMetaStorage } from '../../../../common/image-meta.storage';

const imageDownloadController = (req: Request, res: Response, next: NextFunction) => {
  const id = req.params.id;
  const meta = imageMetaStorage.get(id);

  if (!meta || meta.processing.status !== 'success') {
    return next(new NotFoundException());
  }

  const imagePath = join(Config.outFiles, meta.processing.name!);
  const read = createReadStream(imagePath);

  return read.pipe(res);
};

export { imageDownloadController };
