import { extname } from 'node:path';
import { Request, Response } from 'express';
import { imageProcessingQueue } from '../../../../common/image-processing-queue';
import { ImageMeta, imageMetaStorage, ProcessStatus } from '../../../../common/image-meta.storage';

const imageUploadController = (req: Request, res: Response) => {
  const file = req.file!;
  const format = req.body.format!;

  const nameWithExt = file.filename;
  const nameWithoutExt = nameWithExt.replace(extname(nameWithExt), '');

  const meta: ImageMeta = {
    id: nameWithoutExt,
    name: nameWithoutExt,
    fullName: nameWithExt,
    processing: {
      format,
      operation: 'convert',
      status: ProcessStatus.NoProcessed,
    },
  };

  imageMetaStorage.set(nameWithoutExt, meta);
  imageProcessingQueue.add({ id: nameWithoutExt });

  res.json({ id: meta.id });
};

export { imageUploadController };
