import { NextFunction, Request, Response } from 'express';
import { BadRequestException } from '../../../../common/exceptions';

const SUPPORTED_FORMATS: string[] = ['png', 'webp', 'jpeg', 'jpg'];

const imageUploadValidator = (req: Request, res: Response, next: NextFunction) => {
  const { format } = req.body;

  if (SUPPORTED_FORMATS.includes(format)) {
    return next();
  }

  const error = new BadRequestException('Unsupported format for convert');
  res.status(error.statusCode).json({ errorMessage: error.message });
};

export { imageUploadValidator };
