import { Request, Response } from 'express';
import { NotFoundException } from '../../common/exceptions';

export function notFoundController(req: Request, res: Response) {
  const error = new NotFoundException();
  res.status(error.statusCode).send({ errorMessage: error.message });
}
