class Exception extends Error {
  constructor(readonly statusCode: number, readonly message: string) {
    super();
  }
}

class BadRequestException extends Exception {
  constructor(message?: string) {
    super(400, message ?? 'Bad Request');
  }
}

class NotFoundException extends Exception {
  constructor(message?: string) {
    super(404, message ?? 'Not found');
  }
}

class InternalServerException extends Exception {
  constructor(message?: string) {
    super(500, message ?? 'Internal Server Error');
  }
}

export { InternalServerException, Exception, BadRequestException, NotFoundException };
