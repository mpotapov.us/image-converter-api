import { join } from 'node:path';
import { Config } from '../config/config';
import { convertImageToFormat } from '../routes/images/image.service';
import { DoneHandler, TaskHandler, TaskProcessingStatus, TaskQueue } from '../lib/task-queue';
import { imageMetaStorage, ImageProcessingFailure, ImageProcessingSuccess, ProcessStatus } from './image-meta.storage';

type ImageProcessingTask = { id: string };
type ImageProcessingSuccessData = { name: string };

const processHandler: TaskHandler<ImageProcessingTask> = async ({ id }, cb) => {
  const meta = imageMetaStorage.get(id);

  if (!meta) {
    return cb(new Error('Image Meta not found'), null);
  }

  if (meta.processing.operation !== 'convert') {
    return cb(new Error('Unsupported operation'), null);
  }

  const name = `${meta.name}.${meta.processing.format}`;
  const src = join(Config.srcFiles, meta.fullName);
  const out = join(Config.outFiles, name);

  try {
    imageMetaStorage.update(id, { processing: ProcessStatus.Processing });
    await convertImageToFormat(src, out, meta.processing.format);
    const data: ImageProcessingSuccessData = { name };

    cb(null, data);
  } catch (e) {
    cb(e, null);
  }
};

const doneHandler: DoneHandler<ImageProcessingTask> = (payload) => {
  const meta = imageMetaStorage.get(payload.task.id);

  if (!meta) {
    return;
  }

  if (payload.status === TaskProcessingStatus.Success) {
    const name = (payload.data as ImageProcessingSuccessData).name;

    const processing: ImageProcessingSuccess = {
      ...meta.processing,
      name,
      status: ProcessStatus.Success,
    };

    return imageMetaStorage.update(meta.id, { processing });
  }

  if (payload.status === TaskProcessingStatus.Failure) {
    const processing: ImageProcessingFailure = {
      ...meta.processing,
      error: payload.error,
      status: ProcessStatus.Failure,
    };

    return imageMetaStorage.update(meta.id, { processing });
  }
};

const imageProcessingQueue = new TaskQueue<ImageProcessingTask>(3, {
  process: processHandler,
  done: doneHandler,
});

export { imageProcessingQueue };
