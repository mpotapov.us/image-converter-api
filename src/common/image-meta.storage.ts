import { MemoryStorage } from '../lib/memory-storage';
import { SupportedFormat } from '../routes/images/image.service';

enum ProcessStatus {
  NoProcessed = 'noProcessed',
  Processing = 'processing',
  Success = 'success',
  Failure = 'failure',
}

interface BaseImageProcessing {
  status: ProcessStatus;
  operation: 'convert';
  format: SupportedFormat;
}

interface ImageProcessingPending extends BaseImageProcessing {
  status: ProcessStatus.NoProcessed | ProcessStatus.Processing;
}
interface ImageProcessingSuccess extends BaseImageProcessing {
  status: ProcessStatus.Success;
  name: string;
}

interface ImageProcessingFailure extends BaseImageProcessing {
  status: ProcessStatus.Failure;
  error: unknown;
}

type ImageProcessing = ImageProcessingPending | ImageProcessingSuccess | ImageProcessingFailure;

interface ImageMeta {
  id: string;
  name: string;
  fullName: string;
  processing: ImageProcessing;
}

const imageMetaStorage = new MemoryStorage<ImageMeta>();

export { imageMetaStorage, ProcessStatus, ImageProcessingSuccess, ImageProcessingFailure, ImageProcessing, ImageMeta };
