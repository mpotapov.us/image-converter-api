import morgan from 'morgan';
import express from 'express';
import { routes } from './routes';
import { errorHandlerMiddleware } from './midlewares/error-handler.middleware';
import { healthController } from './routes/common/health.controller';
import { notFoundController } from './routes/common/not-found.controller';

export const app = express();
app.use(morgan('dev'));
app.use('/api', routes);
app.use('/health', healthController);
app.use('*', notFoundController);

app.use(errorHandlerMiddleware);
