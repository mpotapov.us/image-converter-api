import { join } from 'node:path';

const Config = {
  srcFiles: join(__dirname, '..', '..', 'files-src'),
  outFiles: join(__dirname, '..', '..', 'files-out'),
};

export { Config };
