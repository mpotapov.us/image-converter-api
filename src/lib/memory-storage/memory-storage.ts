class MemoryStorage<T> {
  private storage = new Map<string, T>();

  get(id: string): T | null {
    return this.storage.get(id) ?? null;
  }

  set(id: string, data: T) {
    this.storage.set(id, data);
  }

  update<T>(id: string, data: Partial<T> = {}) {
    const item = this.get(id);

    if (!item) {
      throw new Error('Entity not exist');
    }

    this.storage.set(id, { ...item, ...data });
  }
}

export { MemoryStorage };
