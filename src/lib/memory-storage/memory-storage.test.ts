import { MemoryStorage } from './memory-storage';

describe('MemoryStorage', () => {
  let storage: MemoryStorage<{ name: string }>;

  beforeEach(() => {
    storage = new MemoryStorage();
  });

  it('Set value', () => {
    storage.set('1', { name: 'John Doe' });
    expect(storage.get('1')).toEqual({ name: 'John Doe' });
  });

  it('Return null for unknown id', () => {
    expect(storage.get('unknown')).toBeNull();
  });

  it('Update value', () => {
    storage.set('1', { name: 'John Doe' });
    storage.update('1', { name: 'Jane Doe' });
    expect(storage.get('1')).toEqual({ name: 'Jane Doe' });
  });

  it('Throw error when updating unknown id', () => {
    expect(() => {
      storage.update('unknown', { name: 'Jane Doe' });
    }).toThrowError('Entity not exist');
  });
});
