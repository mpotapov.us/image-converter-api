import { TaskQueue } from './task-queue';
import { TaskProcessingStatus } from './task-queue.types';

describe('TaskQueue', () => {
  let taskQueue: TaskQueue<number>;
  let processSpy: jest.Mock;
  let doneSpy: jest.Mock;

  beforeEach(() => {
    processSpy = jest.fn();
    doneSpy = jest.fn();

    taskQueue = new TaskQueue<number>(2, {
      process: processSpy,
      done: doneSpy,
    });
  });

  it('Executes tasks up to the specified concurrency', () => {
    taskQueue.add(1);
    taskQueue.add(2);
    taskQueue.add(3);

    expect(processSpy).toHaveBeenCalledTimes(2);
    expect(processSpy).toHaveBeenCalledWith(1, expect.any(Function));
    expect(processSpy).toHaveBeenCalledWith(2, expect.any(Function));

    // Task 3 should be waiting.
    expect(doneSpy).not.toHaveBeenCalled();
  });

  it('Executes waiting tasks after previously executing tasks complete', () => {
    taskQueue.add(1);
    taskQueue.add(2);
    taskQueue.add(3);

    expect(processSpy).toHaveBeenCalledTimes(2);

    // Complete the first task.
    processSpy.mock.calls[0][1](null, 'successData');
    expect(doneSpy).toHaveBeenCalledWith({
      status: TaskProcessingStatus.Success,
      task: 1,
      data: 'successData',
    });

    // Task 3 should now be executed.
    expect(processSpy).toHaveBeenCalledWith(3, expect.any(Function));
  });

  it('Calls done with failure status if task processing fails', () => {
    taskQueue.add(1);
    taskQueue.add(2);

    expect(processSpy).toHaveBeenCalledTimes(2);

    // Fail the first task.
    processSpy.mock.calls[0][1](new Error('Something went wrong'), null);
    expect(doneSpy).toHaveBeenCalledWith({
      status: TaskProcessingStatus.Failure,
      task: 1,
      error: new Error('Something went wrong'),
    });
  });
});
