import path from 'node:path';
import { nanoid } from 'nanoid';
import { Request } from 'express';
import multer, { FileFilterCallback } from 'multer';
import { Config } from '../config/config';
import { BadRequestException } from '../common/exceptions';

const BYTES_IN_MB = 1048576;
const FILE_SIZE_LIMIT = BYTES_IN_MB * 50;
const SUPPORTED_MIME_TYPES = ['image/jpeg', 'image/png'];

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    return cb(null, Config.srcFiles);
  },
  filename: (req, file, cb) => {
    const extname = path.extname(file.originalname);
    const name = `${nanoid()}.${extname}`;
    return cb(null, name);
  },
});

const fileFilter = (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => {
  if (SUPPORTED_MIME_TYPES.includes(file.mimetype)) {
    cb(null, true);
  } else {
    cb(new BadRequestException('Unsupported MIME type'));
  }
};
export const MulterConfig: multer.Options = {
  storage,
  fileFilter,
  limits: { fileSize: FILE_SIZE_LIMIT },
};

export const imageUploadMiddleware = multer(MulterConfig).single('file');
