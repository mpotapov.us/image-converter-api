import { NextFunction, Request, Response } from 'express';
import { Exception, InternalServerException } from '../common/exceptions';

const errorHandlerMiddleware = (error: unknown, req: Request, res: Response, next: NextFunction) => {
  const err = error instanceof Exception ? error : new InternalServerException();

  res.status(err.statusCode).json({ errorMessage: err.message });
};

export { errorHandlerMiddleware };
