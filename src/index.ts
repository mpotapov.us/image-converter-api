import { createServer } from 'node:http';
import { app } from './app';
import { Config } from './config/config';
import { existsSync, mkdirSync } from 'fs';

const PID = process.pid;
const PORT = process.env.PORT || 3000;

if (!existsSync(Config.srcFiles)) {
  mkdirSync(Config.srcFiles, { recursive: true });
}

if (!existsSync(Config.outFiles)) {
  mkdirSync(Config.outFiles, { recursive: true });
}

const server = createServer(app);

server.listen(PORT, () => console.log(`Server running on port: ${PORT}; PID: ${PID}`));

process.on('unhandledRejection', (error) => {
  console.error('unhandledRejection', error);
});
