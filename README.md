# Supported Methods

#### Upload image uses body { file: File, format: string }

POST /api/images/upload

#### Get image status by id

GET /api/images/status/:id

#### Download image by id

GET /api/images/download/:id

#### Server health check

GET /health

### Also, you can use postman

image-converte-api.postman_collection.json
